import '../styles/globals.css'
import {ReactQueryDevtoolsPanel} from "react-query/devtools";
import {QueryClient, QueryClientProvider} from "react-query";

const queryClient = new QueryClient()

function MyApp({Component, pageProps}) {
    return <QueryClientProvider
        client={queryClient}>
        <Component {...pageProps} />
        <ReactQueryDevtoolsPanel/>
    </QueryClientProvider>
}

export default MyApp
