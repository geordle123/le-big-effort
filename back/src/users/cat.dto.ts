export class CatDto {
  name: string;
  num: number;
  kind?: string;
  aDate?: Date;
}
