import { CatDto } from './cat.dto';

export class Dto {
  name: string;
  dto?: CatDto;
}
