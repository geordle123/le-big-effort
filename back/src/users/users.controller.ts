import { Body, Controller, Param, Query } from '@nestjs/common';
import { ApiController, GetMethod, PostMethod } from '../global/operations';
import { Dto } from './users.dto';
import { ApiImplicitQuery } from '@nestjs/swagger/dist/decorators/api-implicit-query.decorator';

@ApiController('users')
export class UsersController {
  @GetMethod()
  getHello(): Dto {
    return { name: 'hello' };
  }

  @GetMethod(':id')
  getItem(@Param('id') id?: string): Dto {
    return { name: id };
  }

  @ApiImplicitQuery({
    name: 'foo',
    required: false,
    type: String,
  })
  @PostMethod()
  postItem(@Query('foo') foo?: string): Dto {
    return { name: '' };
  }
}
