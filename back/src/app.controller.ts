import { Controller } from '@nestjs/common';
import { AppService } from './app.service';
import { ApiOAuth2 } from '@nestjs/swagger';
import { GetMethod } from './global/operations';

@ApiOAuth2(['pets:write'])
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @GetMethod('one')
  getHello(): number[] {
    return [2, 1];
  }
}
