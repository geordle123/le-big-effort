import {
  DocumentBuilder,
  SwaggerCustomOptions,
  SwaggerModule,
} from '@nestjs/swagger';
import { INestApplication } from '@nestjs/common';
import * as fs from 'fs';

const options: SwaggerCustomOptions = {
  swaggerOptions: {
    persistAuthorization: true,
  },
};
export const setupSwagger = (app: INestApplication) => {
  const config = new DocumentBuilder()
    .setTitle('Cats example')
    .setDescription('The cats API description')
    .setVersion('1.0')
    .addBearerAuth()
    .build();

  const document = SwaggerModule.createDocument(app, config);
  fs.writeFile(
    'resx/swagger.json',
    JSON.stringify(document, null, 2),
    (err) => {
      err && console.log(`unable to write swagger file ${err}`);
    },
  );
  SwaggerModule.setup('/api/docs', app, document, options);
};
