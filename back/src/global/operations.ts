import {
  applyDecorators,
  Controller,
  Delete,
  Get,
  Patch,
  Post,
  Put,
} from '@nestjs/common';
import { ApiOkResponse, ApiResponseOptions, ApiTags } from '@nestjs/swagger';

type Args = Parameters<typeof Get>[0];

const withReturnTypeFix = (method: typeof Post) => {
  return (params?: Args, options?: ApiResponseOptions) =>
    applyDecorators(method(params), FixReturnType(options));
};

export const GetMethod = withReturnTypeFix(Get);
export const PostMethod = withReturnTypeFix(Post);
export const PatchMethod = withReturnTypeFix(Patch);
export const DeleteMethod = withReturnTypeFix(Delete);
export const PutMethod = withReturnTypeFix(Put);

export const ApiController = (prefix?: Args) =>
  applyDecorators(
    Controller(prefix),
    ApiTags(...(Array.isArray(prefix) ? prefix : [prefix])),
  );

const FixReturnType = (options?: ApiResponseOptions): MethodDecorator => (
  target,
  propertyKey,
  descriptor,
) => {
  const returnName = Reflect.getMetadata(
    'design:returntype',
    target,
    propertyKey,
  ).name;

  const contentSchema = getContentSchemaForReturn(returnName);
  return (
    contentSchema &&
    ApiOkResponse({
      ...options,
      content: {
        ...options?.content,
        ...contentSchema,
      },
    })(target, propertyKey, descriptor)
  );
};

const getTypeSchema = function (type: string) {
  return {
    'text/html': {
      schema: {
        type,
      },
    },
  };
};

function getContentSchemaForReturn(returnName: string) {
  const type = returnName.toLowerCase();

  if (['string', 'number'].includes(type)) {
    return getTypeSchema(type);
  }
}
