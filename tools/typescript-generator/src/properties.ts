import {SchemaObject} from "@nestjs/swagger/dist/interfaces/open-api-spec.interface";
import ts from "typescript"
import {isRefObject, OpenApiType} from "./openapi-assertions";

export const getPropertySignatures = (typeMapper: (spec: OpenApiType) => ts.TypeNode) => (schema: SchemaObject) => {
    return getProperties(schema)
        .map(mapPropertyTypes(typeMapper))
        .map(mapToTsProperty);
}

export const getProperties = (schema: SchemaObject) => {
    const requiredProperties = schema.required || [];
    return Object.entries(schema.properties)
        .map(mapRequired(requiredProperties))
}

const mapPropertyTypes = (typeMapper: (schema: OpenApiType) => ts.TypeNode) => ({value, name, required}: ReturnType<ReturnType<typeof mapRequired>>) => ({
    name,
    required,
    type: typeMapper(value)
});

export let mapRequired = (required: string[]) => ([name, value]: [string, OpenApiType]) => (
    {
        name,
        value,
        required: required.includes(name)
    }
);
export const mapToTsProperty = (content: {
    name: string,
    type: ts.TypeNode,
    required: boolean
}) => {
    return ts.createPropertySignature(
        undefined,
        ts.createIdentifier(content.name),
        !content.required ? ts.createToken(ts.SyntaxKind.QuestionToken) : undefined,
        content.type,
        undefined
    )
}
export const mapOpenapiTypeToTsType = (typeRefs: Map<string, string>) => {

    const createRef = (ref: string) => createReferenceTypeNode(typeRefs.get(ref));

    const typeMap = {
        ["string"]: typeKeyWord(ts.SyntaxKind.StringKeyword),
        ["number"]: typeKeyWord(ts.SyntaxKind.NumberKeyword)
    }
    typeMap["array"] = arrayItemType(typeMap, createRef)

    return (spec: OpenApiType): ts.TypeNode => {
        if (isRefObject(spec)) {
            return createRef(spec.$ref);
        } else {
            return typeMap[spec.type](spec)
        }
    };
}

export const createReferenceTypeNode = (name: string): ts.TypeReferenceNode => {
    return ts.createTypeReferenceNode(ts.createIdentifier(name), undefined);
}

export const arrayItemType = (typeMap: TypeNodes, typeRefs: (ref: string) => ts.TypeReferenceNode) => (itemName: SchemaObject) => {
    if (isRefObject(itemName.items)) {
        return typeRefs(itemName.items.$ref)
    }
    return ts.createArrayTypeNode(typeMap[itemName.items.type]())
}
export const typeKeyWord = (type: Parameters<typeof ts.createKeywordTypeNode>[0]) => {
    return (): ts.TypeNode => ts.createKeywordTypeNode(type);
};
export type TypeNodes = { [p: string]: () => ts.TypeNode };


