import {OpenAPIObject} from "@nestjs/swagger";
import fs from "fs"
import ts from "typescript"
import {getPropertySignatures, mapOpenapiTypeToTsType} from "./properties";
import {isRefObject, OpenApiType} from "./openapi-assertions";
import {createControllers} from "./controller";

const file: OpenAPIObject = JSON.parse(fs.readFileSync("./swagger.json").toString());

const toInterface = (propertySignatureMapper: (schema: OpenApiType) => ts.TypeElement[]) => ([name, schema]: [string, OpenAPIObject]) => {
    if (!isRefObject(schema)) {
        return ts.createInterfaceDeclaration(
            undefined,
            [ts.createModifier(ts.SyntaxKind.ExportKeyword)],
            ts.createIdentifier(name),
            undefined,
            undefined,
            propertySignatureMapper(schema)
        );
    }
};

const createImport = () => {
    return ts.createImportDeclaration(
        undefined,
        undefined,
        ts.createImportClause(
            undefined,
            ts.createNamedImports([
                ts.createImportSpecifier(undefined, ts.createIdentifier('fetchClient')),
                ts.createImportSpecifier(
                    undefined,
                    ts.createIdentifier('mapResponseType')
                )
            ])
        ),
        ts.createStringLiteral('./sources')
    )
}

const generateClient = (file: OpenAPIObject) => {

    const entries = Object.entries(file.components.schemas);
    const typeRefs = new Map(entries.map(([name])=> [`#/components/schemas/${name}`, name]));
    const typeMapper = mapOpenapiTypeToTsType(typeRefs);
    const interfaces = entries.map(toInterface(getPropertySignatures(typeMapper)));
    const controller = createControllers(file.paths, typeMapper);
    const apiVariable = ts.createVariableDeclaration(
        ts.createIdentifier('Api'),
        undefined,
        ts.createObjectLiteral(
            controller,
            false
        )
    );
    const rootVariable = [
        ts.createVariableStatement(
            [ts.createModifier(ts.SyntaxKind.ExportKeyword)],
            ts.createVariableDeclarationList(
                [
                    apiVariable
                ],
                ts.NodeFlags.Const
            )
        )
    ]

    return [createImport(), ...interfaces, ...rootVariable];
}


export const writeToString = (generateClient1: ts.Statement[]) => {
    let source = ts.createSourceFile("d.ts", "", ts.ScriptTarget.ES2015);
    source = ts.updateSourceFileNode(source, generateClient1);
    const printer = ts.createPrinter({});
    return printer.printFile(source);
}

const generateClient1 = generateClient(file);



fs.writeFileSync("generated/union.ts", writeToString(generateClient1));
