import {createOperation, flattenPathOperations, OperationGroup} from "./controller";
import {PathsObject} from "@nestjs/swagger/dist/interfaces/open-api-spec.interface";
import _ from "lodash"

const paths = {
    "/api/one": {
        "get": {
            "operationId": "AppController_getHello"
        }
    },
    "/api/users": {
        "get": {
            "operationId": "UsersController_getHello",
            "tags": [
                "users"
            ]
        },
        "post": {
            "operationId": "UsersController_postItem",
            "tags": [
                "users"
            ]
        }
    },
    "/api/users/{id}": {
        "get": {
            "operationId": "UsersController_getItem",
            "tags": [
                "users"
            ]
        }
    }
} as unknown as PathsObject


it('should map pivoted on tags', function () {
    const paths1 = _.cloneDeep(paths);
    paths1["/api/users/{id}"].get.tags.push("one");
    const flattenPathOperations1 = flattenPathOperations(paths1);

    expect(flattenPathOperations1.length).toBe(5);
});

it('should map correctly', function () {
    const flattenPathOperations1 = flattenPathOperations(paths);

    expect(flattenPathOperations1.length).toBe(4);
});


const operations: OperationGroup = [
    ["first", [{
        "operation": "post",
        "operationId": "UsersController_postItem",
        "parameters": [],
        "responses": {
            "200": {
                "description": "",
                "content": {
                    "application/json": {
                        "schema": {
                            "$ref": "#/components/schemas/Dto"
                        }
                    }
                }
            }
        },
        "tags": [
            "users"
        ],
        "path": "/api/users",
        methodName: "",
        controllerName: "",
        outerDescription: undefined,
        outerParams: undefined,
        outerSummary: undefined
    }]]
]


it('should create operations', function () {
    const [key, value] = operations[0];
    createOperation(value[0])
});
