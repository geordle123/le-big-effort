import {ReferenceObject, SchemaObject} from "@nestjs/swagger/dist/interfaces/open-api-spec.interface";
import ts from "typescript"
export type OpenApiType = SchemaObject | ReferenceObject;

export const isRefObject = (object: ReferenceObject | object): object is ReferenceObject => {
    return object && "$ref" in object;
}

export const kindToString = (kind: ts.SyntaxKind) => {
    return ts.SyntaxKind[kind]
}
