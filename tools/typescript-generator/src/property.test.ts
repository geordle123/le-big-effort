import {mapToTsProperty, getProperties, mapRequired, mapOpenapiTypeToTsType} from "./properties";
import ts from "typescript";
import {kindToString} from "./openapi-assertions";


const schema = {
    "CatDto": {
        "type": "object",
        "properties": {
            "name": {
                "type": "string"
            },
            "lol": {
                "type": "number"
            }
        },
        "required": [
            "name"
        ]
    }
}


const firstProperty = Object.entries(schema.CatDto.properties)[0];
it.each([
    [{arr: ["name"]}, true],
    [{arr: []}, false]
])('should map required properties', function (requiredProperties: {arr: string[]}, isRequired: boolean) {
    const {required} = mapRequired(requiredProperties.arr)(firstProperty);
    expect(required).toBe(isRequired)
});

it('should return property specifications', function () {
    const properties = getProperties(schema.CatDto);
    expect(properties.length).toBe(2)
    expect(properties[0].required).toBe(true)
});

it('should create required property', function () {
    //Arrange
    const tsType = mapOpenapiTypeToTsType(new Map<string, string>())({
        type: "string"
    });

    //Act
    const property = mapToTsProperty({name: "name", required: true, type: tsType});

    //Assert
    const {text} = property.name as {text: string};
    expect(text).toBe("name")
    expect(property.questionToken).toBeUndefined()

});

it('should create optional property', function () {
    //Arrange
    const propertySpec = mapRequired([])(firstProperty);
    const tsType = mapOpenapiTypeToTsType(new Map<string, string>())({
        type: "string"
    });

    //Act
    const property = mapToTsProperty({...propertySpec, type: tsType});

    //Assert
    const {text} = property.name as {text: string};
    expect(text).toBe("name")
    expect(property.questionToken).toBeDefined()

});

it('should create string property', function () {
    //Act
    const property = mapOpenapiTypeToTsType(new Map<string, string>())({
        type: "string"
    });
    //Assert
    expect(kindToString(property.kind)).toBe(kindToString(ts.SyntaxKind.StringKeyword))
});

it('should create number property', function () {

    //Act
    const property = mapOpenapiTypeToTsType(new Map<string, string>())({
        type: "number"
    });
    //Assert
    expect(kindToString(property.kind)).toBe(kindToString(ts.SyntaxKind.NumberKeyword))
});


it.each([["string", "number"]])('should create array property with primitives', function (type) {

    //Act
    const property = mapOpenapiTypeToTsType(new Map<string, string>())({
        type: "array",
        items: {
            type: type
        }
    });
    //Assert
    expect(kindToString(property.kind)).toBe(kindToString(ts.SyntaxKind.ArrayType))
});


it('should create ref property', function () {

    //Act
    const property = mapOpenapiTypeToTsType(new Map<string, string>([["$ref/one", "OneDto"]]))({
        $ref: "$ref/one"
    });
    //Assert
    expect(kindToString(property.kind)).toBe(kindToString(ts.SyntaxKind.TypeReference))
});
