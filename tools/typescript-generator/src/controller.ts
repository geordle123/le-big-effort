import ts from "typescript"
import {
    OperationObject,
    ParameterObject,
    PathsObject,
    ReferenceObject, ResponsesObject
} from "@nestjs/swagger/dist/interfaces/open-api-spec.interface";
import _ from "lodash";
import {isRefObject, OpenApiType} from "./openapi-assertions";
import {typeKeyWord} from "./properties";

const operationsTypes = ["get", "put", "post", "delete", "patch", "head"]

export const flattenPathOperations = function (paths: PathsObject) {
    return Object.entries(paths)
        .map(([path, value], index) => ({
            path,
            ...value
        }))
        .flatMap(value => Object
            .entries(value)
            .filter(([key]) => operationsTypes.includes(key))
            .map(([key, innerValue]) => ({
                ...(innerValue as OperationObject),
                operation: key,
                path: value.path,
                outerParams: value.parameters || [],
                outerDescription: value.description,
                outerSummary: value.summary,
            }))
        ).flatMap(value => {

            const [controllerName, methodName] = value.operationId.split("_");
            return {
                ...value,
                methodName,
                controllerName,
            };
        });
};

const pathOperationsGroupedByTag = (paths: PathsObject): [string, ReturnType<typeof flattenPathOperations>][] => {
    const group = flattenPathOperations(paths);
    return Object.entries(_.groupBy(group, el => el.controllerName));
}

export type OperationGroup = ReturnType<typeof pathOperationsGroupedByTag>

export const createControllers = function (paths: PathsObject, typeMapper: (spec: OpenApiType) => ts.TypeNode) {
    const groupByTag = pathOperationsGroupedByTag(paths);
    return groupByTag.map(([key, value]) => {
        const operations = value.map(createOperation(typeMapper));
        return ts.createPropertyAssignment(
            ts.createIdentifier(key),
            ts.createObjectLiteral(
                operations,
                false
            )
        );
    });

};

function getResponse(responses: ResponsesObject, typeMapper: (spec: OpenApiType) => ts.TypeNode) {
    const OkResponse = responses["200"];
    if (isRefObject(OkResponse)) {
        return typeMapper(OkResponse);
    }
    const jsonResponse = OkResponse.content["application/json"].schema;
    return typeMapper(jsonResponse);
}

export const createOperation = (typeMapper: (spec: OpenApiType) => ts.TypeNode) => (value: ReturnType<typeof flattenPathOperations>[0]) => {
    const parameters = [...value.outerParams, ...value.parameters];
    const mapToParameter = createParameter(typeMapper);
    const map = parameters
        .map(value1 => value1 as ParameterObject)
        .map(value1 => mapToParameter(value1.name, value1.required)(value1.schema));

    if (value.requestBody && !isRefObject(value.requestBody)) {
        const body = mapToParameter("body", true)(value.requestBody.content["application/json"].schema);
        map.push(body);
    }

    return ts.createMethod(
        undefined,
        [ts.createModifier(ts.SyntaxKind.AsyncKeyword)],
        undefined,
        ts.createIdentifier(value.methodName),
        undefined,
        undefined,
        map.sort(a => a.questionToken ? -1 : 1),
        ts.createTypeReferenceNode(ts.createIdentifier('Promise'), [getResponse(value.responses, typeMapper)]),
        createFetch()
    );
};

const createFetch = () => {
    return ts.createBlock(
        [
            ts.createReturn(
                ts.createCall(
                    ts.createPropertyAccess(
                        ts.createCall(
                            ts.createPropertyAccess(
                                ts.createCall(
                                    ts.createIdentifier('fetchClient'),
                                    undefined,
                                    [
                                        ts.createStringLiteral('/api/:one'),
                                        ts.createArrayLiteral(
                                            [
                                                ts.createObjectLiteral(
                                                    [
                                                        ts.createPropertyAssignment(
                                                            ts.createIdentifier('path'),
                                                            ts.createStringLiteral('one')
                                                        ),
                                                        ts.createPropertyAssignment(
                                                            ts.createIdentifier('type'),
                                                            ts.createStringLiteral('query')
                                                        ),
                                                        ts.createPropertyAssignment(
                                                            ts.createIdentifier('value'),
                                                            ts.createStringLiteral('')
                                                        )
                                                    ],
                                                    false
                                                )
                                            ],
                                            false
                                        )
                                    ]
                                ),
                                ts.createIdentifier('get')
                            ),
                            undefined,
                            []
                        ),
                        ts.createIdentifier('then')
                    ),
                    undefined,
                    [
                        ts.createCall(
                            ts.createIdentifier('mapResponseType'),
                            undefined,
                            [ts.createStringLiteral('application/json')]
                        )
                    ]
                )
            )
        ],
        true
    )
}

const createParameter = (typeMapper: (spec: OpenApiType) => ts.TypeNode) => (name: string, required: boolean) => (parameter: OpenApiType) => {
    return ts.createParameter(
        undefined,
        undefined,
        undefined,
        ts.createIdentifier(name),
        required ? undefined : ts.createToken(ts.SyntaxKind.QuestionToken),
        typeMapper(parameter),
        undefined
    );
};
