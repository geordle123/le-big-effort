type ParameterType = {
    path: string,
    type: "query" | "path",
    value: string | number | boolean
}

const formatUrl = (url: string, parameters: ParameterType[]) => {
    for (let parameter of parameters.filter(value => value.type === "path")) {
        url.replace(parameter.path, encodeURIComponent(parameter.value))
    }
    const queryParams = parameters.filter(value => value.type === "query")
        .map(value => ([value.path, encodeURIComponent(value.value)]));
    const query = new URLSearchParams(queryParams).toString();
    return url + "?" + query
}

const fetchInit = (method: string) => (body?: object) => {
    return fetch("", {
        method,
        body: body && JSON.stringify(body),
        credentials: "include",
    })
}

export const fetchClient = (url: string, parameters: ParameterType[]) => {
    return {
        get: fetchInit("GET"),
        post: fetchInit("POST"),
        patch: fetchInit("PATCH"),
        put: fetchInit("PUT"),
        delete: fetchInit("DELETE"),
    }
}


class UnexpectedResponseTypeError extends Error {
    private body: unknown;

    constructor(expected: string, received: string, body: unknown) {
        super(`Fetch returned unexpected content-type. Expected: "${expected}"` +
            ` but received:"${received}". Please check if generated Typescript client is up-to-date`);
        this.name = "UnknownResponseTypeError"
        this.body = body;
    }
}

type ResponseContentType = "text/html" | "application/json";

const mapResponseType = (expectedType: ResponseContentType) => async (response: Response) => {

    const receivedType = response.headers.get("content-type").split(";")[0];
    if (receivedType !== expectedType) {
        throw new UnexpectedResponseTypeError(expectedType, receivedType, await response.text())
    }

    switch (expectedType) {
        case "application/json":
            return response.json();
        case "text/html":
            return response.text()
    }
}

const ensureSuccess = (response: Response) => {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}
