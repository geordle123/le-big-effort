FROM golang:1.16

RUN cd /go && \
    go get -u filippo.io/mkcert

WORKDIR /root/.local/share/mkcert

COPY execute.sh /

CMD ["/bin/bash", "/execute.sh"]
