Welcome, you've decided to join me eh. How brave of you. Before you lies the product of my blood, sweat, tears and pasta-pesto. 

Starting it should be easy as `docker-compose up -V --build`. You might want to run `npm install` to install deps for intellisense. 
  
if you want to be super fancy and remove all those red cert warning things you can trust the CA located at `.localdev/generated/certs/rootCA.pem` 

Please complain heavily if stuff breaks
