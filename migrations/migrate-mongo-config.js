const config = {
  mongodb: {
    url: "mongodb://mongodb:27017",

    databaseName: "le-big-effort",

    options: {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  },
  migrationsDir: "migrations",
  changelogCollectionName: "changelog",
  migrationFileExtension: ".js",
  useFileHash: false
};

// Return the config as a promise
module.exports = config;
